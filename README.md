# My Sample Java EE 6 Application on Openshift

To start the application with an embedded JBoss AS, simply run the following command:

    mvn package jboss-as:run


----

**2013 Micha Kops / hasCode.com**
