package com.hascode.nodes.controller;

import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.apache.commons.lang3.StringUtils;
import org.jboss.logging.Logger;

import com.hascode.nodes.entity.LogEntry;
import com.hascode.nodes.entity.Note;
import com.hascode.nodes.service.LogBean;
import com.hascode.nodes.service.NoteBean;

@ManagedBean
@RequestScoped
public class ShellController {
	private final Logger log = Logger.getLogger(getClass());

	@EJB
	private LogBean logBean;

	@EJB
	private NoteBean noteBean;

	public String runCommand(String command, String[] params) {
		StringBuilder optString = new StringBuilder();
		for (String opt : params) {
			optString.append(opt);
			optString.append(", ");
		}
		log.info("running command '" + command + "' with params '["
				+ optString.toString() + "]'");

		if (command.equals("date"))
			return new Date().toString();

		if (command.equals("log") && "show".equals(params[0]))
			return printLogs();

		if (command.equals("note") && "add".equals(params[0]))
			return saveNote(params);

		if (command.equals("note") && "show".equals(params[0]))
			return printNotes();

		if (command.equals("help"))
			return printHelp();

		return "command " + command + " not found, run 'help' for help.";
	}

	private String saveNote(String[] params) {
		if (params.length < 2 || params[1] == null || params[1].isEmpty())
			return "wrong syntax, correct is: note add message";

		params[0] = "";
		String message = StringUtils.join(params, " ");

		Note note = new Note();
		note.setMessage(message);
		noteBean.save(note);

		return "note saved. run 'note show' to display the latest notes";
	}

	private String printHelp() {
		return

		"Available commands: date, log show, help, note show, note add";
	}

	private String printNotes() {
		StringBuilder sb = new StringBuilder();
		for (Note n : noteBean.getLatest()) {
			sb.append(String.format("> (%s) %s<br/>", n.getCreated(),
					n.getMessage()));
		}
		return sb.toString();
	}

	private String printLogs() {
		StringBuilder sb = new StringBuilder();
		for (LogEntry e : logBean.latestLogs()) {
			sb.append(String.format("> (%s) %s<br/>", e.getCreated(),
					e.getMessage()));
		}
		return sb.toString();
	}
}
