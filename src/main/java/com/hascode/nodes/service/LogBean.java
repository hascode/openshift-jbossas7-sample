package com.hascode.nodes.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hascode.nodes.entity.LogEntry;

@Stateless
public class LogBean {
	@PersistenceContext
	private EntityManager em;

	public List<LogEntry> latestLogs() {
		return em.createNamedQuery(LogEntry.QUERY_LATEST, LogEntry.class)
				.getResultList();
	}
}
