package com.hascode.nodes.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hascode.nodes.entity.Note;

@Stateless
public class NoteBean {
	@PersistenceContext
	private EntityManager em;

	public void save(Note note) {
		em.persist(note);
	}

	public List<Note> getLatest() {
		return em.createNamedQuery(Note.QUERY_LATEST, Note.class)
				.getResultList();
	}
}
