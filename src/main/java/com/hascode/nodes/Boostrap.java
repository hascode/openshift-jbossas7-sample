package com.hascode.nodes;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.hascode.nodes.entity.LogEntry;

@Startup
@Singleton
public class Boostrap {
	@PersistenceContext
	private EntityManager em;

	@PostConstruct
	private void initialize() {
		LogEntry log = new LogEntry();
		log.setMessage("application booted");
		em.persist(log);
	}
}
